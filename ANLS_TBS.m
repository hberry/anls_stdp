
function [w,allsol]=ANLS_TBS(parin)
global tstim_post_spike currstepon tstim_pre attenuation freq


% ANLS_TBS - This function generates Theta Burst Stimulations STDP (TBS) 
% and accounts for ATP-control of STDP expression.
%
% USAGE: [w,allsol]=ANLS_TBS(parin)
%
% INPUT: parin    - a vector containing 8 parameters:
%                 parin(1) sets the spike timing (in seconds)
%                 parin(2) sets the number of theta bursts
%                 parin(3) sets the initial value of the internal variable rho
%                 parin(4) indicates the addition of oxamate : 
%                          0 for no oxamate
%                          1 for added oxamet (inhibition of LDH by 90%)                  
%                 parin(5) sets the external Glucose concentration (mM)
%                 parin(6) sets the total NADH+NAD+ concentration in the patch pipette (mM)
%                 parin(7) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
%                 parin(8) is a verbosity option; if plotoption=0, nothing is plotted at all   
%                  - if parin is empty or has less or more than 8 elements, values by defaults are used (line 57)  
%
% OUTPUT: w      - a column vector giving the value of the synpatic weight at each integration step
%         allsol - a structure contaning the results of model integration, in particular with fields:
%                       - allsol.x: integration times
%                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
%                       - see matlab help on ODE15s for the other fields       
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal


%%%%%%%% ALL CONCENTRATIONS in mM  and TIME in seconds %%%%%%%%%%%%



%%%% input parameters %%%
if numel(parin)==8
    dtstdp=parin(1);%spike timing (in seconds)
    n_TBS=parin(2);% number of theta bursts applied 
    rho0=parin(3);% initial value of the internal variable rho
    oxamate=parin(4);% oxamate concentration: 
                     % =0 fo no oxamate
                     %=1 for inhibition of LDH by 90%
    GLCc=parin(5);%external [Gluc] (mM)
    NADtn=parin(6);% total NADH+NAD+ concentration in the patch pipette (mM)
    APtn=parin(7);%total ATP+ADP+AMP concentration in the patch pipette (mM)
    plotoption=parin(8);%if plotoption=0, nothing is plotted at all
else %values by default
    dtstdp=0.01;
    n_TBS=5;
    rho0=0;
    oxamate=0;
    GLCc=5;
    NADtn=0.212;
    APtn=4;
    plotoption=1;    
end

% intensity of the depolarizing current injected in the postsynaptic neuron 
DPmaxincr=0;

% input parameters for the ODE solver
paramODEs=[oxamate, GLCc, NADtn, APtn,DPmaxincr];


%%%%% Output options
plotvalidate=1;% to check with plots that the parameters agree with experimental controls 
plotstab=0;%to check with plots that the steady state has been reached before (for the neuron) and after the stimulations



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%   PARAMETERS  %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%  tLTP section %%%%%%%%%%%%
freq=1;% not used for TBS
currstepon=0;% not used for TBS
ns_max=4;% number of presynaptic stimulations per bursts 
attenuation =0.34;% attenuation of the postynaptic AP wrt its value in the soma
DPdur=0.01;% not used for TBS (no injected current in the postsynaptic neuron)
APamp=130;%amplitude of the postsynpatic bAP, in mV, in the soma
%APamp is slightly increased in TBS wrt STDP to compensate for the absence of depol
APmax=APamp*attenuation;%amplitude of the bAP at the synapse
delta1=0.007;% delay between depol and spike  
tD=1.00;% time of the onset of the first depolarization
tpre1=tD+delta1-dtstdp;%time of the first presynaptic spike
per =1/freq; % not used for TBS
tmax=tD+ns_max*per+2700;% max simulation time 
betarho=0.55;%proportionality constant between rho and the synaptic weight
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial conditions = steady states of the non-stimulated system

if oxamate==1
    y0=load('data/IC_ANLS_STDP_ox.mat','-ascii');
    %the file IC_ANLS_STDP_ox.mat contains the steady-state values of the
    %variables with oxamate
else
    y0=load('data/IC_ANLS_STDP_ctrl.mat','-ascii');
    %the file IC_ANLS_STDP_ctrtl.mat contains the steady-state values of the
    %variables in the absence of oxamate
end
y0(31)=rho0;


% y0=[
%  1 Nan    
%  2 Naa  
%  3 GLCn  
%  4 GLCa  
%  5 GAPn
%  6 GAPa
%  7 PEPn
%  8 PEPa
%  9 PYRn
%  10 PYRa
%  11 LACn
%  12 LACa
%  13 NADHcn
%  14 NADHca
%  15 NADHmn
%  16 NADHma
%  17 ATPn
%  18 ATPa
%  19 PCrn
%  20 PCra 
%  21 O2n
%  22 O2a
%  23 GLCe
%  24 LACe
%  25 Istep-----------------------\
%  26 Vn                           |
%  27 CacytN                       |
%  28 s_CaL                         > 6 ODEs for the Vm-Ca-STDP system (h_Na is not part of it)    
%  29 u_CaL                        |  25 ODEs for the ANLS system.
%  30 h_Na                         |
%  31 internal signal state rho---/
%         ];

if plotoption
    disp('%%%%%%%% Pre-equilibration %%%%%%%%');
end

%%%%%%%% Equilibration before stimulation %%%%%
tmaxeq=2e5;%max time for pre-équilibration
tstim_pre=[];%no stimulation
tstim_post_spike=[];%no stimulation

options = odeset('RelTol',1e-10,'AbsTol',1e-10);

% numerical solution of the ODE system
sol=ode15s(@(t,y)STDP(t,y,paramODEs),[0 tmaxeq],y0,options);


% the final state after equilibration becomes initia conditions of the
% stimulation part
y0=sol.y(:,end);
y0(31)=rho0;

if plotoption
    disp('%%%%%%%% Stimulation %%%%%%%%');
end

%%%%%%%  5TBS STIMULATIONS %%%%%%

st=0:1:(ns_max-1);
%period between two intraburst presynaptic spikes  (100 Hz)
perintra=1/100;

% presynaptic stimulations for
% n_TBS bursts of ns_max spikes at 100 Hz, repeated 10 times at 5 Hz
% with one burst every 10s (burst freq = 0.1 Hz)
for stim=0:10:(10*(n_TBS-1))
    for burst=0:9
        tstim_pre=[tstim_pre, tpre1+stim+0.2*burst+(st*perintra)];
    end
end

% tstim_pre contains the times of presynaptic spikes
% tstim_posts contains the times of postsynaptic spikes
tstim_post_spike=tstim_pre+dtstdp;

% But on average, only one bAP is tiggered for two presynaptic spike
tstim_post_spike=tstim_post_spike(1:2:end);

% the options passed to the ODE solve now account for eventss
options = odeset('Events',@events,'RelTol',1e-10,'AbsTol',1e-10);


tstart=0;
% the numerical results will be stored in allsol
allsol=[];

while (tstart<tmax)
    tspan=[tstart tmax];
    sol=ode15s(@(t,y)STDP(t,y,paramODEs),tspan,y0,options);
    %accumulation of the integration results in allsol
    if isempty(allsol)
        allsol=sol;
    else
        allsol.x=[allsol.x, sol.x];
        allsol.y=[allsol.y, sol.y];
    end
    % update of the intial conditions for a new integration
    y0=sol.y(:,end);
    tstart=sol.x(end);
   
    % the integration is stopped at postsynaptic spike time, 
    % and the spike is emulated by the addition of APmax mV to the 
    % postynpatic voltage y(26)
    if ~isempty(sol.ie)
        y0(26)=y0(26)+APmax;
        % go to the next postsynaptic spike time
        tstim_post_spike=circshift(tstim_post_spike,-1);
    end

end


T=(allsol.x/60)';% time in minutes

%Compute the synaptic weight from variable rho
rho=(allsol.y(31,:))';
w=(1+betarho*rho)*(1-rho0)+rho0*(0.7+0.3*rho);


%generate_plots plots the main variables that are shown in figure 2B,C of
%the paper

%the file 'dataexp_5TBS' contains the experimental data shown in Figu 2B
%(for 5TBS )
if plotoption
    generate_plots(allsol,w,plotvalidate,plotstab,500,'data/dataexp_5TBS','bAP.mat')
end
end

% the function for even detection
function [value,isterminal,direction] = events(t,y)
    global tstim_post_spike
    value= t-tstim_post_spike(1);   
    isterminal = 1;   % Stop the integration at the next postsynaptic spike time
    direction =  1;   % positive direction only
end




