function generate_plots(sol,w,plotvalidate,plotstab,TM,infw,bAP)

%
% generate_plots - plots a load of data from the ODE system 
%
%
% USAGE: generate_plots(allsol,w,plotvalidate,plotstab,TM,infw,bAP)
%
% INPUT:    allsol              - a structure contaning the results of model integration, that must at least contain the fields:
%                               - allsol.x: integration times
%                               - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
%           w               - a column vector giving the value of the synpatic weight at each integration step
%           plotvalidate    - nothing is plotted if plotvalidate=0
%           plotstab        - a flag, nothing happens, of plotvalidate=0
%                             plotvalidate=1 plots a set of curves that can
%                             be used to check that the ssteady state has
%                             indeed been reached
%           TM              - max time for the plots
%           infw            - name of the file that gives the experimental
%                             values of the evolution in time of the synaptic weight
%           bAP             - name of the file that gives the experimental
%                             values of the evolution in time of the
%                             voltage of the bAP as measured in the soma
%   

% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal

global attenuation

%%%%Experimental data in control conditions freq=0.5Hz, 50 stims dt=+10ms
dataraw=dlmread(infw);
[texp,expdata]=averagedata(dataraw,1,45);


T=(sol.x/60)';

if plotvalidate 
    subplot(5,2,1), plot(sol.x,sol.y(26,:),'b'), legend('Vn (mV)');xlim([0 TM]);
    subplot(5,2,2), plot(sol.x,sol.y(1,:),'b');   ylabel('Nan (mM)');xlim([0 TM]);
    subplot(5,2,3), plot(sol.x,sol.y(2,:),'r'); ylabel('Naa (mM)');xlim([0 TM]);
    subplot(5,2,4), plot(sol.x,sol.y(19,:),'b',sol.x,sol.y(20,:),'r'); legend('PCrn','PCra');xlim([0 TM]);
    subplot(5,2,5), plot(sol.x,sol.y(17,:),'b',sol.x,sol.y(18,:),'r'); legend('ATPn','ATPa');xlim([0 TM]);
    subplot(5,2,6), plot(sol.x,(sol.y(14,:)-sol.y(14,1))/sol.y(14,1)*100,'r',sol.x,(sol.y(15,:)-sol.y(15,1))/sol.y(15,1)*100,'b'); legend('NADHca (%)','NADHmn (%)');xlim([0 TM]);
    subplot(5,2,7), plot(sol.x,sol.y(13,:),'b'); legend('NADHcn (mM)');xlabel('time (sec)');xlim([0 TM]);
    subplot(5,2,8), plot(sol.x,(sol.y(24,:)-sol.y(24,1))/sol.y(24,1)*100,'k'); legend('LACe (%)');xlabel('time (sec)');xlim([0 TM]);
    subplot(5,2,9), plot(sol.x,sol.y(11,:)); legend('LACn (mM)');xlabel('time (sec)');xlim([0 TM]);
    subplot(5,2,10), plot(sol.x,sol.y(3,:)); legend('GLCn (mM)');xlabel('time (sec)');xlim([0 TM]);
    figure;
    subplot(4,1,1), plot(sol.x,sol.y(25,:)), legend('Istep (post)');xlim([0 10]);
    subplot(4,1,2), plot(sol.x,sol.y(26,:)), ylabel('Vm (mV)');xlim([0 10]);
    subplot(4,1,3), plot(sol.x,sol.y(27,:));ylabel('Ca (mM)');xlim([0 10]);%xlim([0.8 2]);ylim([0 2])
    subplot(4,1,4), plot(sol.x,sol.y(31,:)), xlabel('time (min)'),ylabel('Syn Weight');
    
    %The SplitPlot template is givven after the curret code
    figure;SplitPlot(T,sol.y(1,:),'b','Nan (mM)',[-inf inf]);
    figure;SplitPlot(T,sol.y(11,:),'b','LACn (mM)',[-inf inf]);
    figure;SplitPlot(T,sol.y(3,:),'b','GLCn (mM)',[-inf inf]);
    figure;SplitPlot(T,sol.y(17,:),'b','ATPn (mM)',[-inf inf]);
    figure;SplitPlot(T,w*100,'b','syn weight ',[-inf inf]);
    
    figure();
    %warning: the fit with the bAP was done at the soma (no attenuation)
    %and in the absence of any presynaptic stimulation
    %the following plot compares model and data at the synapse (with
    %attenuation) and, potentially with a presynap  tic signal
    plot(texp,expdata,'o',T,w);
    figure();
    plot(sol.x,sol.y(26,:)), ylabel('synaptic Vm (mV)');xlim([0.95 1.3]);
    hold on;
    plot(bAP(:,1)+.996,bAP(1,2)+(bAP(:,2)-bAP(1,2))*attenuation,'o');
end
if plotstab %used to check that the value are indeed stable
    figure;
    for i=1:12
        subplot(4,3,i), plot(sol.x,sol.y(i,:)/sol.y(i,1)*100);legend(num2str(i));
    end
    sgtitle('Post-stimulation period');
    figure;
    for i=1:12
        subplot(4,3,i), plot(sol.x,sol.y(12+i,:)/sol.y(12+i,1)*100);legend(num2str(12+i));
    end
    sgtitle('Post-stimulation period');
end

disp(['Ca peak amplitude= ' num2str((max(sol.y(27,:))-sol.y(27,end))*1e3) ' microM']);
disp(['ATPn min= ' num2str(min(sol.y(17,:))) ' microM']);
disp(['NADHcn max= ' num2str(max(sol.y(13,:))) ' microM']);
disp(['NADHcn min= ' num2str(min(sol.y(13,:))) ' microM']);
end


function SplitPlot(x,y,st,leg,ymM)

t = tiledlayout(1,2,'TileSpacing','compact');
bgAx = axes(t,'XTick',[],'YTick',[],'Box','off');
bgAx.Layout.TileSpan = [1 2];
ax1 = axes(t);
ax1.Layout.Tile=1;

plot(ax1,x,y,st);ylim(ymM);
xline(ax1,3,':');
ax1.Box = 'off';
xlim(ax1,[-0.1 3])
legend(leg);

ax2 = axes(t);
ax2.Layout.Tile = 2;
plot(ax2,x,y,st);ylim(ymM);
xline(ax2,18,':');
ax2.YAxis.Visible = 'off';
ax2.Box = 'off';
xlim(ax2,[18 40])


% Link the axes
linkaxes([ax1 ax2], 'y')


end