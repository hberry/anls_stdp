function [tres,datares]=averagedata(rawdata,dt,tmax)


% averagedata - an utility to average the time series rawdata with x-bin size 
% given by dt and maximial value tmax
%
%
% USAGE: [tres,datares]=averagedata(rawdata,dt,tmax)
%
% INPUT:rawdata - a vector containing the data to average in columns
%               - WARNING: the first column must give the time steps
%       dt      - the bin width for the aveaging                         
%       tmax    - the largest value of the bins

% OUTPUT: tres      - a column vector of the bins ie tres=(1:dt:tmax)'
%         datares   - the averaged results, ie datares(i) gives the mean of 
%                   the columns 2 to end of rawdata for which the first column
%                   rawdata(:,1) is in [tres(i)-dt/2, tres(i)+dt/2]                
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal


tres=(1:dt:tmax)';
datares=zeros(length(tres),1);
for i=1:length(tres)
    curt=tres(i);
    interval=find(rawdata(:,1)>=curt-dt/2 & rawdata(:,1)<curt+dt/2);
    % the line numbers for which time ie rawdata(:,1) is larger or equal to 
    % tres(i)-dt/2 and smaller than tres(i)+dt/2
    if isempty(interval)% if no points in this interval
        if i>1 %not the first time bin
            datares(i)=datares(i-1);
        else
            datares(i)=0;
        end
    else
        % if interval is not empty, carry out the averaging
        datares(i)=mean(mean(rawdata(interval,2:end)));
    end
end


end