
% makemaps - This script build 2D maps of plasticty (2-parameter plots)
% like the one shown figure 2D.   
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal

clear;

%%% fixed Parameters %%%
freq=1;%stimulation frequency
dtstdp=0.01;% spike timing
rho0=0;% initial value of the internal parameter rho
oxamate=1;% 0= no oxamate; 1= inhibition of the neuronal LDH by 90% 
plotoption=0;% no plots
GLCc=5;% extracellular glucose concentration
NADtn=0.212;%0.212 mM in control;4 mM for i-NADH addition in the pipette
APtn=4;%Total ATP+ADP+AMPP content

par1name='\# of theta bursts';%first parameter of the map (on the y-axis)
start_par1=1;
n_points_par1=6;
incr_par1=1;

par2name='[Glucose]c (mM)';%2nd parameter of the map (on the x-axis)
start_par2=3;
n_points_par2=23;
incr_par2=1;

range_par1=start_par1+(0:n_points_par1)*incr_par1;
range_par2=start_par2+(0:n_points_par2)*incr_par2;
wmat=zeros(length(range_par1),length(range_par2));


%parin=[dtsdtp, ns_max,freq,rho0, oxamate, GLCc, plotoption]
for p1=1:1:n_points_par1
   for p2=1:1:n_points_par2
       disp(['(',num2str(range_par1(p1)),',',num2str(range_par2(p2)),')']);
       %params for TBS: [dtsdtp, n_TBS, rho0, oxamate, GLCc, NADtn, APtn, plotoption]
       %params for STFP: [dtsdtp, ns_max,freq,rho0, oxamate, GLCc, NADtn, APtn, plotoption]
       parin=[0.01,range_par1(p1),rho0, 1,range_par2(p2), NADtn, APtn, plotoption];
       [w,res]=ANLS_TBS(parin);  
       %[w,res]=ANLS_STDP(parin); 
       disp(num2str(w(end)));
       wmat(p1,p2)=w(end);
   end
end

% plot the map
imagesc(range_par2,range_par1,wmat*100);ylabel(par1name,'Interpreter','latex');xlabel(par2name,'Interpreter','latex');
title('syn. weight change (\%), STDP 1 Hz Ctrl ','Interpreter','latex');
colormap(flipud(gray));colorbar;caxis([100 140.5]);
set(gca,'YDir','normal');