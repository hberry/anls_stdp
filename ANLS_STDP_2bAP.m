function [w,allsol]=ANLS_STDP_2bAP(parin)


% ANLS_STDP_2bAP - This function generates STDP stimulations with two bAP per postsynaptic
%depolarization and accounts for ATP-control of STDP expression.
%
% 
% USAGE: [w,allsol]=ANLS_STDP_2bAP(parin)
%
% INPUT: parin    - a vector containing 9 parameters:
%                 parin(1) sets the spike timing (in seconds)
%                 parin(2) sets the number of pre-post pairings
%                 parin(3) sets the frequency of the prepost pairings (Hz)
%                 parin(4) sets the initial value of the internal variable rho
%                 parin(5) indicates the addition of oxamate : 
%                          0 for no oxamate
%                          1 for added oxamet (inhibition of LDH by 90%)                  
%                 parin(6) sets the external Glucose concentration (mM)
%                 parin(7) sets the total NADH+NAD+ concentration in the patch pipette (mM)
%                 parin(8) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
%                 parin(9) is a verbosity option; if plotoption=0, nothing is plotted at all   
%                  - if parin is empty or has less or more than 9 elements, values by defaults are used (line 57)  
%
% OUTPUT: w      - a column vector giving the value of the synpatic weight at each integration step
%         allsol - a structure contaning the results of model integration, in particular with fields:
%                       - allsol.x: integration times
%                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
%                       - see matlab help on ODE15s for the other fields       
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal


%%%%%%%% ALL CONCENTRATIONS in mM  and TIME in seconds %%%%%%%%%%%%


global tstim_post_ston tstim_post_stoff tstim_post_spike currstepon tstim_post_spike2 tstim_pre attenuation freq


%%%% input parameters %%%
if numel(parin)==9
    dtstdp=parin(1);%spike timing (in seconds)
    ns_max=parin(2);%number of pre-post pairings
    freq=parin(3);% frequency of the prepost pairings
    rho0=parin(4);%initial value of the internal variable rho
    oxamate=parin(5);% oxamate concentration: 
                     % =0 fo no oxamate
                     %=1 for inhibition of LDH by 90%     
    GLCc=parin(6);%external [Gluc] (mM)
    NADtn=parin(7);% total NADH+NAD+ concentration in the patch pipette (mM)
    APtn=parin(8);%total ATP+ADP+AMP concentration in the patch pipette (mM)
    plotoption=parin(9);%if plotoption=0, nothing is plotted at all    
else %values by default
    dtstdp=0.01;
    ns_max=50;
    freq=0.5;
    rho0=0;
    oxamate=0;
    GLCc=5;
    NADtn=0.212;
    APtn=4;
    plotoption=1;    
end

% additional intensity of the depolarizing current injected in the postsynaptic neuron
% compared to 1 bAP per depol
DPmaxincr=1.9;

% input parameters for the ODE solver
paramODEs=[oxamate, GLCc, NADtn, APtn,DPmaxincr];

%%%%% Output options
plotvalidate=1;% to check with plots that the parameters agree with experimental controls 
plotstab=0;%to check with plots that the steady state has been reached before (for the neuron) and after the stimulations


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%   PARAMETERS  %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%  tLTP section %%%%%%%%%%%%
attenuation =0.34;% attenuation of the bAP from the soma to the recorded synapse
DPdur=0.01;% duration of the postsynpatic depolarization (in sec)
APamp=114;%amplitude of the postsynpatic bAP, in mV, in the soma
APmax=APamp*attenuation;%amplitude of the bAP at the synapse
delta1=0.0055;% delay between depol and first spike
currstepon=0;%=1 if postynaptic current injection is currently ongoing 
            %in the soma, 0 else
tD=1.00;% time of the onset of the first depolarization
tpre1=tD+delta1-dtstdp;%time of the first presynaptic spike
per =1/freq;%period
tmax=tD+ns_max*per+2700;% max simulation time
betarho=0.405;%proportionality constant between rho and the synaptic weight
%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial conditions = steady states of the non-stimulated system

y0=zeros(31,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial conditions = steady states of the non-stimulated system

if oxamate==1
    y0=load('data/IC_ANLS_STDP_ox.mat','-ascii');
    %the file IC_ANLS_STDP_ox.mat contains the steady-state values of the
    %variables with oxamate
else
    y0=load('data/IC_ANLS_STDP_ctrl.mat','-ascii');
    %the file IC_ANLS_STDP_ctrtl.mat contains the steady-state values of the
    %variables in the absence of oxamate    
end

% y0=[
%  1 Nan    
%  2 Naa  
%  3 GLCn  
%  4 GLCa  
%  5 GAPn
%  6 GAPa
%  7 PEPn
%  8 PEPa
%  9 PYRn
%  10 PYRa
%  11 LACn
%  12 LACa
%  13 NADHcn
%  14 NADHca
%  15 NADHmn
%  16 NADHma
%  17 ATPn
%  18 ATPa
%  19 PCrn
%  20 PCra 
%  21 O2n
%  22 O2a
%  23 GLCe
%  24 LACe
%  25 Istep-----------------------\
%  26 Vn                           |
%  27 CacytN                       |
%  28 s_CaL                         > 6 ODEs for the Vm-Ca-STDP system (h_Na is not part of it)    
%  29 u_CaL                        |  25 ODEs for the ANLS system.
%  30 h_Na                         |
%  31 internal signal state rho---/
%         ];

if plotoption
    disp('%%%%%%%% Pre-equilibration %%%%%%%%');
end

%%%%%%%% Equilibration before stimulation %%%%%
tmaxeq=2e5;%max time for pre-équilibration
tstim_pre=[];%no stimulation
tstim_post_spike=[];%no stimulation

options = odeset('RelTol',1e-10,'AbsTol',1e-10);

% numerical solution of the ODE system
sol=ode15s(@(t,y)STDP(t,y,paramODEs),[0 tmaxeq],y0,options);

% the final state after equilibration becomes initia conditions of the
% stimulation part
y0=sol.y(:,end);
y0(31)=rho0;



if plotoption
    disp('%%%%%%%% Stimulation %%%%%%%%');
end

%%%%%%%  STIMULATIONS %%%%%%
% tstim_pre contains the times of presynaptic spikes
% tstim_post_spike contains the times of the 1st postsynaptic spikes
% tstim_post_spike2 contains the times of the 2nd postsynaptic spikes
% tstim_post_ston contains the times of the depolarization onset
% tstim_post_stoff contains the times of the depolarization end

st=0:1:(ns_max-1);
tstim_pre=tpre1+(st*per);
tstim_post_ston=tstim_pre+dtstdp-delta1;
tstim_post_stoff=tstim_post_ston+DPdur;
%First bAP
tstim_post_spike=tstim_pre+dtstdp;
%Second bAP
delta2=0.0063; % delat between the two bAP
tstim_post_spike2=tstim_post_spike+delta2;
attenuation_bAP2=0.65;%attenuation of the 2nd bAP


% the options passed to the ODE solve now account for events
options = odeset('Events',@events,'RelTol',1e-10,'AbsTol',1e-10);


tstart=0;
% the numerical results will be stored in allsol
allsol=[];

while (tstart<tmax)
    tspan=[tstart tmax];
    sol=ode15s(@(t,y)STDP(t,y,paramODEs),tspan,y0,options);
    %accumulation of the integration results in allsol
    if isempty(allsol)
        allsol=sol;
    else
        allsol.x=[allsol.x, sol.x];
        allsol.y=[allsol.y, sol.y];
    end
    % update of the intial conditions for a new integration
    y0=sol.y(:,end);
    tstart=sol.x(end);

    % the integration is stopped :
    % 1) at postsynaptic spike time and the spike is emulated 
    % by the addition of APmax mV to the 
    % postynpatic voltage y(26)
    % 2) at the onset of a postsynpatic depolarization, currstepon is then set to 1 
    % 3)at the terrmination of a postsynpatic depolarization, currstepon is then set back to 0

    if ~isempty(sol.ie)
        if sol.ie(end)==1
            currstepon=1.0;
            % go to the next depolarization onset time
            tstim_post_ston=circshift(tstim_post_ston,-1);
        elseif sol.ie(end)==2
            currstepon=0.0;
            % go to the next depolarization termination time
            tstim_post_stoff=circshift(tstim_post_stoff,-1);
        elseif sol.ie(end)==3
            y0(26)=y0(26)+APmax;
            % go to the next postsynaptic 1st spike time
            tstim_post_spike=circshift(tstim_post_spike,-1);
        elseif sol.ie(end)==4
            % go to the next postsynaptic 2nd spike time
            y0(26)=y0(26)+attenuation_bAP2*APmax;%the 2nd bAP is less tall
            tstim_post_spike2=circshift(tstim_post_spike2,-1); 
        end
    end
    
end


rho=(allsol.y(31,:))';
w=(1+betarho*rho)*(1-rho0)+rho0*(0.7+0.3*rho);

bAP=dlmread('data/bAP_2bAP_exp');
%the file bAP_2bAP_exp contains the time profile of a typical 2bAP stim recorded at the soma
%the file 'dataexp_0p5_50' contains the experimental data shown in Figu 2B
%(for STDP W/ 1 bAP)

%generate_plots plots the main variables that are shown in figure 2B,C of
%the paper
if plotoption
    generate_plots(allsol,w,plotvalidate,plotstab,500,'data/dataexp_0p5_50',bAP)
end
end


function [value,isterminal,direction] = events(t,y)
    global tstim_post_ston tstim_post_stoff tstim_post_spike tstim_post_spike2
    value=[t-tstim_post_ston(1) t-tstim_post_stoff(1) t-tstim_post_spike(1) t-tstim_post_spike2(1)];   
    isterminal = [1 1 1 1];   % Stop the integration
    direction =  [1 1 1 1];   % positive direction only
end



