function dy = STDP(t,y,param)

% 
% STDP - This function expresses the ODE model
%
% USAGE: dy=STDP(t,y,param)
%
% INPUT: t     - the current time step
%        y     - a vector containing the current value of the ODE variables, 
%                eg y(1) is neuronal Na concentration  
%        param - external parameters passed to the ODE function
%                param(1) indicates the addition of oxamate : 
%                   0 for no oxamate
%                   1 for added oxamet (inhibition of LDH by 90%)                  
%                 param(2) sets the external Glucose concentration (mM)
%                 param(3) sets the total NADH+NAD+ concentration in the patch pipette (mM)
%                 param(4) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
%                 param(5) sets a pottential additional intensity of the depolarizing 
%                 current injected in the postsynaptic neuron compared to STDP with 1 bAP per depol       
%
% WARNING:  the presynaptic spike times are passed in the global variable tstim_pre
%           the postsynaptic sike times or event are handled out of the
%           present function since integration stops at every postsynaptic
%           event (onset of the depolarization, postynaptic spike time,
%           termination of the depolarization)
%
% OUTPUT: dy      - the vector of the time derivatives of y       
%        
% AUTHOR: Hugues Berry
%         hugues.berry@inria.fr
%         http://www.inrialpes.fr/Berry/
%
% REFERENCE: DEMBITSKAYA et al. Lactate supply overtakes glucose when neural computational and cognitive load scale up.. 
%
% LICENSE: CC0 1.0 Universal


%%%%%%%% ALL CONCENTRATIONS in mM  and TIME in seconds %%%%%%%%%%%%



global currstepon tstim_pre attenuation freq

% the input parrameters
oxamate=param(1);% oxamate concentration: 
                 % =0 fo no oxamate
                 %=1 for inhibition of LDH by 90%     
GLCc=param(2);%external [Gluc] (mM)
Nn=param(3);% total NADH+NAD+ concentration in the patch pipette (mM)
An=param(4);%total ATP+ADP+AMP concentration in the patch pipette (mM)
DPmaxincr=param(5);% additional intensity of the depolarizing current injected in the postsynaptic neuron
                   % compared to 1 bAP per depol
                   
% the vector of derivatives dy/dt                   
dy=zeros(31,1);





%%%%%%%              %%%%%%%%
%%%%%    PARAMETERS      %%%%
%%%%%%%              %%%%%%%%
Nae=150;%mM external Na concentation
Cao=5e3;% mM external Ca concentation 
RToverF=26.73;%mV RT/F
ENan=RToverF*log(Nae/y(1)); % Nernst potential for Na in neurons
ENaa=RToverF*log(Nae/y(2));% Nernst potential for Na in astrocytes
SmVxoverF=0.259107;%mole/cm/C surface to volume ratios divided by F 
SmVx=2.5e4;%cm-1 % surface to volume ratios 

%Parameters of the AMPAR
tauAMPA1=9.6e-3;% time constant 1
tauAMPA2=7e-3;%time constant 2
gAMPAmax=0.13;% max AMAP conductantce
    %set so that each pre spike increases V from around 2 mV, with an
    %increase time around 9 ms and a decay time scale around 10 ms too
    %cf Watanabe et al (2002); figure 2A

% the AMPA current   
preAMPA=sum(exp(-(t-tstim_pre).*double(t>=tstim_pre)/tauAMPA1).*double(t>=tstim_pre)-exp(-(t-tstim_pre).*double(t>=tstim_pre)/tauAMPA2).*double(t>=tstim_pre));
gAMPA=gAMPAmax*preAMPA;
IAMPA=gAMPA*y(26);


%Parameters of the NMDAR
tauNMDA1=60e-3;% time constant 1
tauNMDA2=1.0e-3;% time constant 2

% the NMDA current 
preNMDA=sum(exp(-(t-tstim_pre).*double(t>=tstim_pre)/tauNMDA1).*double(t>=tstim_pre)-exp(-(t-tstim_pre).*double(t>=tstim_pre)/tauNMDA2).*double(t>=tstim_pre));
gNMDAmax=4.6e-04;
Mg=1;%nM
B=1/(1+Mg/3.57*exp(-0.062*y(26)));

gNMDA=gNMDAmax*preNMDA;
INMDA=B*gNMDA*y(26);

%tauNMDA1 and tauNMDA2 are set so that the time-change of NMDAR-transported (ie whithout CaL)
%Ca behaves in agreement with corresponding experimental observation
%in Sabatini et al, 2002 ie a rapide rise then decay phase
%that needs around 200 ms to converge back to baseline. 

% gNMDAmax is set so that each pre spike increases Ca of circa
% 0.17 microM at -70 mV (Sabatini et al, 2002)


%Paramters of the VDCC - CaL From Solinas et al, 2014
alphas=50*exp((y(26)+29.06)/15.9);
betas=80*exp(-(y(26)+18.66)/25.6);
alphau=exp(-(y(26)+48)/18.2);
betau=exp((y(26)+48)/83);
gCaLmax=0.0849;%S
ECa=54;%mV%the theoretivcal value (according to eg Nernst, ie around 130 mV) but
% differential permeabilities to Ca2+ and K+ ions cause the reversal potential
%to deviate from the predicted Nernst potential of ~130 mV to a less polarized value
%of around + 54 mV see eg Solinas et al 2014

ICaL=gCaLmax*y(28)*y(28)*y(29)*(y(26)-ECa);

% Parameters of the Ca influx through VDCC (bAP)
% set so that the bAP make \Delta Ca = 400 nM in the synapse (Sabatini Carter 2004) 

%Parameters of the neuronal Na current (from Jolivet 2015)
alpham=-0.1*(y(26)+33)/(exp(-0.1*(y(26)+33))-1);
betam=4*exp(-(y(26)+58)/12);
minf=alpham/(alpham+betam);
alphah=0.07*exp(-(y(26)+50)/10);
betah=1/(exp(-0.1*(y(26)+20))+1);
gNa=11.5;%--> modified to account for the change in stimulation and Vm model compared to Jolivet (40 dans Jolivet)
% in particular in Jolivet, the Vn model is HH with time dependent ENa which show spike-frequency adaptation 
% and stop emitting spikes after 7 s even though the excitatory stimulation continues

% %%% Here the In Vitro results of Jolivet et al, 2015  (that come from Kasische_2004) 
% are reproduced with
%%% dtstdp=0.01 (10 ms) and 64 pairings at 3.2 Hz i.e. 
%%% ANLS_STDP([0.01,64,3.2,0,0,5,1]);

INa=gNa*minf^3*y(30)*(y(26)-ENan);


%Parameters of the dendrite
Cm=1e-3;%%mF/cm2
gL=0.50;%118.37;% mS/cm2 --> increased from Jollivet (was 0.02) to fit the bAP
EL=-70;% mV

%Stimulation parameters
% warning DPmax mustt be increased with 2 bAP to account for 
% the increased amplitude of the depol in 2bAP
DPmax=DPmaxincr*1500*attenuation;
taustep=0.015;
Istep=currstepon*DPmax;

% set so as to mimick our typical 1bAP or 2bAP + depolarizations see files
% ie in the soma (no attenuation) cf FigurebAP_calibre.pdf and file bAP.mat
% then attenuated

%Specific Ca parameters
tauCa=0.04;

   
% Synaptic weight
% Set by fitting w on the exp. data in control 0.5 Hz, 50 bAP, in dataexp_0p5_50
taurho=103.15;
rhostar= 0.257;

%Thresholds for LTD/LTP
LTDstart=0.45e-3;% set by the range of LTD (dt_stdp in [-25, -0] ms for 100 stims)
LTPstart=0.74e-3;%et by the range of LTP (dt_stdp in [0, 30] ms for 100 stims) 
LTDMax=70;%% Set by fitting w on the exp. data in control 0.5 Hz, 50 bAP, in dataexp_0p5_50
LTPMax=120;%% Set by fitting w on the exp. data in control 0.5 Hz, 50 bAP, in dataexp_0p5_50 
%ATP-control of the signaling patway from Ca to synaptic weight
Mmax=30;
ATPthr=1.87;%Set to suppress LTP for (5TBS ) in oxamate cf file conc_avant_thresh 

%%%%%%%%%  ANLS Section %%%%%%%%%%%%%%%%%%c

%Fixed parameters From Jollivetlivet et al
gNan=0.0136;%mS/cm2 
gNaa=0.0061;%mS/cm2
Vma=-70;%mV
kpumpn=2.2e-6;%cm mM-1 s-1 
kpumpa=4.5e-7;%cm mM-1 s-1
Kmpump=0.5;
Ktg=8;
Tgca=0.0016;
Tgen=0.041;
Tgea=0.147;
Tgce=0.239;
kHKPFKn=0.0504;%0.0504e-3 for inhibition by Mannoheptulose but not smaller than 0.01 if inhibition by mannoheptulose + oxamate (numerical issues)
kHKPFKa=0.185;
Kg=0.05;
kPGKn=3.97;
kPGKa=135.2;
Na=0.212;
Aa=2.212;%total ATP+ADP+AMP contentration is 2.212 mM by default in the astro
Cn=10;%total phosphocreatine in the patch pipette
Ca=10;%total phosphocreatine in the astro
qAK=0.92;
kPKn=36.7;
kPKa=401.7;
% parameters for LDH activity
kLDHonn=72.3*10^(-oxamate);
kLDHoffn=0.72*10^(-oxamate);
kLDHona=1.59;
kLDHoffa=0.071;
LACc=0.55;
Tlne=24.3;
Tlae=106.1;
Tlac=0.00243;
Tlec=0.25;
Ktlne=0.74;
Ktlae=3.5;
Ktlac=1;
Ktlec=1;
Vmitoinn=0.1303;
Vmitoina=5.7;
KMmito=0.04;
KMNADn=0.409;
KMNADa=40.3;
Vmitooutn=0.164;
Vmitoouta=0.064;
KO2mito=0.001;
KMADPn=3.41e-3;
KMADPa=0.483e-3;
KMNADHn=4.44e-2;
KMNADHa=2.69e-2;
TNADHn=10330;
TNADHa=150;
Mcyton=4.9e-8;
Mcytoa=2.5e-4;
Mmiton=3.93e5;
Mmitoa=1.06e4;
kCKonn=0.0433;
kCKoffn=0.00028;
kCKona=0.00135;
kCKoffa=1e-5;
O2c=7;
PScapoverVa=0.87;
PScapoverVn=1.66;
KO2=0.0361;
HbOP=8.6;
oneonh=1/2.73;
Ve=0.2;
Va=0.25;
Vn=0.45;
xi=0.07;
ren=Ve/Vn;
rea=Ve/Va;
JATPasesn=0.1695;
JATPasesa=0.1404;
Jpumpa0=0.0687;

%%%%%%%              %%%%%%%%
%%%%%    ODE S           %%%%
%%%%%%%              %%%%%%%%
%%%% Rate functions for ANLS %%%%

un=qAK^2+4*qAK*(An/y(17)-1);
ua=qAK^2+4*qAK*(Aa/y(18)-1);
ADPn=y(17)/2*(-qAK+sqrt(un));
ADPa=y(18)/2*(-qAK+sqrt(ua));
dAMPATPn=-1+0.5*qAK-0.5*sqrt(un)+qAK*An/(y(17)*sqrt(un));

dAMPATPa=-1+0.5*qAK-0.5*sqrt(ua)+qAK*Aa/(y(18)*sqrt(ua));
Rmn=y(13)/(Nn-y(13));
Rma=y(14)/(Na-y(14));
Rpn=(Nn-y(15))/y(15);
Rpa=(Na-y(16))/y(16);


JleakNan=SmVxoverF*gNan*(ENan-y(26));
JleakNaa=SmVxoverF*gNaa*(ENaa-Vma);
Jpumpn=SmVx*kpumpn*y(17)*y(1)/(1+y(17)/Kmpump);%
Jpumpa=SmVx*kpumpa*y(18)*y(2)/(1+y(18)/Kmpump);
Jgca=Tgca*(GLCc/(GLCc+Ktg)-y(4)/(y(4)+Ktg));
Jgea=Tgea*(y(23)/(y(23)+Ktg)-y(4)/(y(4)+Ktg));
Jgce=Tgce*(GLCc/(GLCc+Ktg)-y(23)/(y(23)+Ktg));
Jgen=Tgen*(y(23)/(y(23)+Ktg)-y(3)/(y(3)+Ktg));
JHKPFKn=kHKPFKn*y(17)*y(3)/(y(3)+Kg)/(1+(y(17))^4);
JHKPFKa=kHKPFKa*y(18)*y(4)/(y(4)+Kg)/(1+(y(18))^4);
JPGKn=kPGKn*y(5)*ADPn*(Nn-y(13))/y(13);
JPGKa=kPGKa*y(6)*ADPa*(Na-y(14))/y(14);
JPKn=kPKn*y(7)*ADPn;
JPKa=kPKa*y(8)*ADPa;
JLDHn=kLDHonn*y(9)*y(13)-kLDHoffn*y(11)*(Nn-y(13));
JLDHa=kLDHona*y(10)*y(14)-kLDHoffa*y(12)*(Na-y(14));
Jlne=Tlne*(y(11)/(y(11)+Ktlne)-y(24)/(y(24)+Ktlne));
Jlae=Tlae*(y(12)/(y(12)+Ktlae)-y(24)/(y(24)+Ktlae));
Jlac=Tlac*(y(12)/(y(12)+Ktlac)-LACc/(LACc+Ktlac));
Jlec=Tlec*(y(24)/(y(24)+Ktlec)-LACc/(LACc+Ktlec));
Jmitoinn=Vmitoinn*y(9)/(y(9)+KMmito)*(Nn-y(15))/(Nn-y(15)+KMNADn);
Jmitoina=Vmitoina*y(10)/(y(10)+KMmito)*(Na-y(16))/(Na-y(16)+KMNADa);
Jmitooutn=Vmitooutn*y(21)/(y(21)+KO2mito)*ADPn/(ADPn+KMADPn)*y(15)/(y(15)+KMNADHn);
Jmitoouta=Vmitoouta*y(22)/(y(22)+KO2mito)*ADPa/(ADPa+KMADPa)*y(16)/(y(16)+KMNADHa);
Jshuttlen=TNADHn*Rmn/(Rmn+Mcyton)*Rpn/(Rpn+Mmiton);
Jshuttlea=TNADHa*Rma/(Rma+Mcytoa)*Rpa/(Rpa+Mmitoa);
JCKn=kCKonn*ADPn*y(19)-kCKoffn*y(17)*(Cn-y(19));
JCKa=kCKona*ADPa*y(20)-kCKoffa*y(18)*(Ca-y(20));
JO2cn=PScapoverVn*(KO2*(HbOP/O2c-1)^(-oneonh)-y(21));
JO2ca=PScapoverVa*(KO2*(HbOP/O2c-1)^(-oneonh)-y(22));

%Jstima and Jstimn are adapted from Jolivet
% cause not the same electrical stimulation 
% So prefa changed from 3*2.25e-5*1500 to
% 2.25e-5*1500 to yield the correct effect on NADHa and ATPa
% Likewise gNa (conductance of the VGSC) decreased 

Jstimn=SmVxoverF*(-2/3*IAMPA-INa);
Jstima=2.25e-5*1500*freq*sum(double(t>=min(tstim_pre))*double(t<=max(tstim_pre)));

%%%% . equations for the ANLS %%%%
%dNan/dt
dy(1)=JleakNan-3*Jpumpn+Jstimn;
%dNaa/dt
dy(2)=JleakNaa-3*Jpumpa+Jstima;
%dGLCn/dt
dy(3)=Jgen-JHKPFKn;
%dGLCa/dt
dy(4)=Jgca+Jgea-JHKPFKa;
%dGAPn/dt
dy(5)=2*JHKPFKn-JPGKn;
%dGAPa/dt
dy(6)=2*JHKPFKa-JPGKa;
%dPEPn/dt
dy(7)=JPGKn-JPKn;
%dPEPa/dt
dy(8)=JPGKa-JPKa;
%dPYRn/dt
dy(9)=JPKn-JLDHn-Jmitoinn;
%dPYRa/dt
dy(10)=JPKa-JLDHa-Jmitoina;
%dLACn/dt
dy(11)=JLDHn-Jlne;
%dLACa/dt
dy(12)=JLDHa-Jlae-Jlac;
%dNADHcn/dt
dy(13)=(JPGKn-JLDHn-Jshuttlen)/(1-xi);
%dNADHca/dt
dy(14)=(JPGKa-JLDHa-Jshuttlea)/(1-xi);
%dNADHmn/dt
dy(15)=(4*Jmitoinn-Jmitooutn+Jshuttlen)/xi;
%dNADHma/dt
dy(16)=(4*Jmitoina-Jmitoouta+Jshuttlea)/xi;
%dATPn/dt
dy(17)=(-2*JHKPFKn+JPGKn+JPKn-JATPasesn-Jpumpn+3.6*Jmitooutn+JCKn)/(1-dAMPATPn);
%dATPa/dt
dy(18)=(-2*JHKPFKa+JPGKa+JPKa-JATPasesa-7/4*Jpumpa+3/4*Jpumpa0+3.6*Jmitoouta+JCKa)/(1-dAMPATPa);
%dPCrn/dt
dy(19)=-JCKn;
%dPCra/dt
dy(20)=-JCKa;
%dO2n/dt
dy(21)=JO2cn-0.6*Jmitooutn;
%dO2a/dt
dy(22)=JO2ca-0.6*Jmitoouta;
%dGLCe/dt
dy(23)=Jgce-Jgea/rea-Jgen/ren;
%dLACe/dt
dy(24)=-Jlec+Jlae/rea+Jlne/ren;
%dh_Na/dt
dy(30)=alphah*(1-y(30))-betah*y(30);


%%%% Equations for the STDP system %%%%
%dIstep/dt
dy(25)=-y(25)/taustep+Istep;
%dV/dt
dy(26)=1/Cm*(-gL*(y(26)-EL)-INMDA-IAMPA-ICaL+y(25));



%dCa_cyt/dt
dy(27)=(-y(27)-SmVxoverF*INMDA-SmVxoverF*ICaL)/tauCa;
%ds_Cal/dt
dy(28)=alphas*(1-y(28))-betas*y(28);
%du_CaL/dt
dy(29)=alphau*(1-y(29))-betau*y(29);

%synaptic weight
dy(31)=(-y(31)*(1-y(31))*(rhostar-y(31))+LTPMax*(1-y(31))*double((y(27)-LTPstart)>=0)-LTDMax*y(31)*double((y(27)-LTDstart)>=0)-Mmax*y(31)*double((ATPthr-y(17))>=0))/taurho;
%double(x>=0) is for heavisde(x)


end