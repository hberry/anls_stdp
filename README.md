# ANLS_STDP

anls_stdp implements the model for the coupling of astrocyte-neuron lactate shuttle (ANLS) with spike-timing dependent plasticity (STDP) published in 

Yulia DEMBITSKAYA, Sylvie PEREZ, Charlotte PIETTE, Hugues BERRY, Pierre J MAGISTRETTI, and Laurent VENANCE. Lactate supply overtakes glucose when neural computational and cognitive load scale up.

The ANLS model is based on the model published in Jolivet R, Coggan JS, Allaman I, Magistretti PJ (2015) Multi-timescale Modeling of Activity-Dependent Metabolic Coupling in the Neuron-Glia-Vasculature Ensemble. PLoS Comput Biol 11(2): e1004036. doi:10.1371/journal. pcbi.1004036. The STDP model is adapted from the ideas of Graupner M and Brunel N (2012) Calcium-based plasticity model explains sensitivity of synaptic changes to spike pattern, rate, and dendritic location, Proceedings of the National Academy of Sciences Mar 2012, 109 (10) 3991-3996; DOI: 10.1073/pnas.1109359109.


anls_stdp is implemented in Matlab (the current version has been written on R2020a). 

# Usage

The package contains the following functions:
- ANLS_STDP.m
- ANLS_STDP_2bAP.m
- ANLS_TBS.m
- makemaps.m
- STDP.m
- generate_plots.m
- averagedata.m


/// ANLS_STDP.m ///

This function generates STDP stimulations with one bAP per postsynaptic
 
 USAGE: [w,allsol]=ANLS_STDP(parin)

 INPUT: parin    - a vector containing 9 parameters:
                 parin(1) sets the spike timing (in seconds)
                 parin(2) sets the number of pre-post pairings
                 parin(3) sets the frequency of the prepost pairings (Hz)
                 parin(4) sets the initial value of the internal variable rho
                 parin(5) indicates the addition of oxamate : 
                          0 for no oxamate
                          1 for added oxamet (inhibition of LDH by 90)                  
                 parin(6) sets the external Glucose concentration (mM)
                 parin(7) sets the total NADH+NAD+ concentration in the patch pipette (mM)
                 parin(8) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
                 parin(9) is a verbosity option; if plotoption=0, nothing is plotted at all   
                  - if parin is empty or has less or more than 9 elements, values by defaults are used (line 57)  

 OUTPUT: w      - a column vector giving the value of the synpatic weight at each integration step
         allsol - a structure contaning the results of model integration, in particular with fields:
                       - allsol.x: integration times
                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
                       - see matlab help on ODE15s for the other fields 

For instance, the simulation results of Figure 2C (STDP experiment with 50 pairings of a single bAP, at 0.5 Hz, spike timing = +10 ms, intial rho=0 and control conditions for all the other parameters) are retrieved by the command:
[w,allsol]= ANLS_STDP([0.01,50,0.5,0,0,5,0.212,4,1]); - in the absence of oxamate, and 
[w,allsol]= ANLS_STDP([0.01,50,0.5,0,1,5,0.212,4,1]); - with oxamate


/// ANLS_STDP_2bAP.m ///

ANLS_STDP_2bAP - This function generates STDP stimulations with two bAP per postsynaptic depolarization and accounts for ATP-control of STDP expression.
 
 USAGE: [w,allsol]=ANLS_STDP_2bAP(parin)

 INPUT: parin    - a vector containing 9 parameters:
                 parin(1) sets the spike timing (in seconds)
                 parin(2) sets the number of pre-post pairings
                 parin(3) sets the frequency of the prepost pairings (Hz)
                 parin(4) sets the initial value of the internal variable rho
                 parin(5) indicates the addition of oxamate : 
                          0 for no oxamate
                          1 for added oxamet (inhibition of LDH by 90)                  
                 parin(6) sets the external Glucose concentration (mM)
                 parin(7) sets the total NADH+NAD+ concentration in the patch pipette (mM)
                 parin(8) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
                 parin(9) is a verbosity option; if plotoption=0, nothing is plotted at all   
                  - if parin is empty or has less or more than 9 elements, values by defaults are used (line 57)  

 OUTPUT: w      - a column vector giving the value of the synpatic weight at each integration step
         allsol - a structure contaning the results of model integration, in particular with fields:
                       - allsol.x: integration times
                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
                       - see matlab help on ODE15s for the other fields 

For instance, the simulation results of Figure 3C (STDP experiment with 50 pairings of 2 bAP, at 1 Hz, spike timing = +10 ms, intial rho=0, with oxamate and control conditions for all the other parameters) are retrieved by the command:
[w,allsol]= ANLS_STDP_2bAP([0.01,50,1,0,1,5,0.212,4,1]) - without added NADH or
[w,allsol]= ANLS_STDP_2bAP([0.01,50,1,0,1,5,4,4,1]) - with 4mM added NADH 


/// ANLS_TBS.m ///

 ANLS_TBSS - This function generates Theta Burst Stimulations STDP (TBS) and accounts for ATP-control of STDP expression.

 USAGE: [w,allsol]=ANLS_TBS(parin)

 INPUT: parin    - a vector containing 8 parameters:
                 parin(1) sets the spike timing (in seconds)
                 parin(2) sets the number of theta bursts
                 parin(3) sets the initial value of the internal variable rho
                 parin(4) indicates the addition of oxamate : 
                          0 for no oxamate
                          1 for added oxamet (inhibition of LDH by 90)                  
                 parin(5) sets the external Glucose concentration (mM)
                 parin(6) sets the total NADH+NAD+ concentration in the patch pipette (mM)
                 parin(7) sets total ATP+ADP+AMP concentration in the patch pipette (mM)
                 parin(8) is a verbosity option; if plotoption=0, nothing is plotted at all   
                  - if parin is empty or has less or more than 8 elements, values by defaults are used (line 57)  

 OUTPUT: w      - a column vector giving the value of the synpatic weight at each integration step
         allsol - a structure contaning the results of model integration, in particular with fields:
                       - allsol.x: integration times
                       - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
                       - see matlab help on ODE15s for the other fields 

For instance, the simulation results of Figure 2C (TBS experiment with 5 theta bursts, spike timing = +10 ms, intial rho=0 and control conditions for all the other parameters) are retrieved by the command:
[w, allsol]=ANLS_TBS([0.01, 5,0,0,5,0.212,4,1]); - in the absence of oxamate, and 
[w, allsol]=ANLS_TBS([0.01, 5,0,0,1,0.212,4,1]); - with oxamate



/// makemaps.m ///

makemaps - This script builds 2D maps of plasticty like the one shown figure 2D.   
        
This 2-parameter plot shows the synaptic wieght reached at the end of the stimulation (tmax parameter in each fonction) depending on the value of parameters par1 (y-axis) and par2 (x-axis).

Specify the values of explored parameters on lines 30 and 35
Insert the call to the desired protocol (ANLS_STDP, ANLS_STDP_2bAP or ANLS_TBS) on lines 47-48


/// generate_plots.m ///

 generate_plots - plots a load of data from the ODE system 

 USAGE: ANLS_TBS(allsol,w,plotvalidate,plotstab,TM,infw,bAP)

 INPUT:    sol              - a structure contaning the results of model integration, that must at least contain the fields:
                               - allsol.x: integration times
                               - allsol.y: integration solution, ie allsol.y(1,:) gives the time evolution of neuronal Na
           w 				- a column vector giving the value of the synpatic weight at each integration step
           plotvalidate    - nothing is plotted if plotvalidate=0
           plotstab        - a flag, nothing happens, of plotvalidate=0
                             plotvalidate=1 plots a set of curves that can
                             be used to check that the ssteady state has
                             indeed been reached
           TM              - max time for the plots
           infw            - name of the file that gives the experimental
                             values of the evolution in time of the synaptic weight
           bAP             - name of the file that gives the experimental
                             values of the evolution in time of the
                             voltage of the bAP as measured in the soma
   



/// averagedata.m ///

 averagedata - an utility to average the time series rawdata with x-bin size 
 given by dt and maximial value tmax


 USAGE: [tres,datares]=averagedata(rawdata,dt,tmax)

 INPUT:rawdata - a vector containing the data to average in columns
               - WARNING: the first column must give the time steps
       dt      - the bin width for the aveaging                         
       tmax    - the largest value of the bins

 OUTPUT: tres      - a column vector of the bins ie tres=(1:dt:tmax)'
         datares   - the averaged results, ie datares(i) gives the mean of 
                   the columns 2 to end of rawdata for which the first column
                   rawdata(:,1) is in [tres(i)-dt/2, tres(i)+dt/2]  












